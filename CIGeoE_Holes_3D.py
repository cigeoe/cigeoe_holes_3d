# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CIGeoEHoles3D
                                 A QGIS plugin
 Draws holes 3D in polygons
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2019-01-25
        git sha              : $Format:%H$
        copyright            : (C) 2019 by Centro de Informação Geoespacial do Exército
        email                : igeoe@igeoe.pt
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt5.QtCore import QSettings, QTranslator, qVersion, QCoreApplication
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction, QMessageBox, QToolBar

#from qgis.gui import QgsMessageBar
from qgis.core import QgsMapLayer, QgsWkbTypes

# Initialize Qt resources from file resources.py
from .resources import *
from .drawPolygon import drawPolygon

# Import the code for the dialog
from .CIGeoE_Holes_3D_dialog import CIGeoEHoles3DDialog
import os.path


class CIGeoEHoles3D:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'CIGeoEHoles3D_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&CIGeoE Holes 3D')

        # Check for CIGeoE toolbar. If exists, add button there; if not exists, create one
        cigeoeToolBarExists = False
        for x in iface.mainWindow().findChildren(QToolBar): 
            if x.windowTitle() == 'CIGeoE':
                self.toolbar = x
                cigeoeToolBarExists = True
        if cigeoeToolBarExists==False:
            self.toolbar = self.iface.addToolBar(u'CIGeoE')

        self.toolbar.setObjectName(u'CIGeoEHoles3D')


        # Check if plugin was started the first time in current QGIS session
        # Must be set in initGui() to survive plugin reloads
        self.first_start = None

        #************* connect to drawpolygon ******************

        self.canvas = self.iface.mapCanvas()        
        self.drawpolygon = drawPolygon(self.canvas, self.iface)    #add iface for work with active layer 

        #*********************************************

        self.isActive = False


    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('CIGeoEHoles3D', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            # Adds plugin icon to Plugins toolbar
            #self.iface.addToolBarIcon(action)
            self.toolbar.addAction(action)                          #++   

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/CIGeoE_Holes_3D/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'CIGeoE Holes 3D : draws holes in 3D in polygons'),
            callback=self.run,
            parent=self.iface.mainWindow())

        # will be set False in run()
        self.first_start = True


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&CIGeoE Holes 3D'),
                action)
            self.iface.removeToolBarIcon(action)


        #********************************************************
        # Unset the map tool in case it's set
        self.iface.mapCanvas().unsetMapTool(self.drawpolygon)

        #*********************************************************

        # remove the toolbar
        del self.toolbar


    def run(self):
        """Run method that performs all the real work"""

        listpoints = []

        layer = self.iface.activeLayer()

        if not layer:
            QMessageBox.information(self.iface.mainWindow(), "Error", 'Layer is not loaded!')
            return  

        if not layer.isEditable():
            QMessageBox.information(self.iface.mainWindow(), "Error", 'Layer is not editable!')
            return  
                    
        #if layer.type() != QgsMapLayer.VectorLayer or layer.geometryType() != QGis.Polygon:     # pyqgis2
        if layer.type() != QgsMapLayer.VectorLayer or layer.geometryType() != QgsWkbTypes.PolygonGeometry:         
            QMessageBox.information(self.iface.mainWindow(), "Error", 'Layer geometries must be of polygon type.')
            return 

        multi_polygons_exist = any([f.geometry().isMultipart() for f in layer.getFeatures()])
        if multi_polygons_exist:
            QMessageBox.information(self.iface.mainWindow(), "Error", 'Layer geometries must be of polygon type and not multipolygon.')
            return 

        if not (layer.wkbType() & QgsWkbTypes.PointZ):#this bitwise operation checks if points have Z dimension
            QMessageBox.information(self.iface.mainWindow(), "Error", 'Layer geometries doesn\'t have Z dimension.')
            return
      
                         
        if self.isActive == False:
            self.iface.mapCanvas().setMapTool(self.drawpolygon)                      
            self.isActive = True
        else:
            self.iface.mapCanvas().unsetMapTool(self.drawpolygon)
            self.isActive = False