# -*- coding: utf-8 -*-

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QMessageBox 

from qgis.gui import QgsMessageBar, QgsMapTool, QgsRubberBand
#from qgis.gui import *
#from qgis.core import QgsWkbTypes, Qgis, QgsPointXY, QgsPoint, QgsPolygon, QgsRectangle, QgsMapLayer, QgsFeatureRequest, QgsGeometry
from qgis.core import *
from qgis.utils import iface 

from math import *

import sys
import re
import math


class drawPolygon(QgsMapTool):
    
    def __init__(self, canvas, iface):  
        
        self.canvas = canvas
        self.iface = iface               # add iface for work with active layer       
        QgsMapTool.__init__(self,self.canvas)        

        #self.rb=QgsRubberBand(self.canvas,QGis.Polygon)                # pyqgis2
        self.rb=QgsRubberBand(self.canvas, geometryType=QgsWkbTypes.PolygonGeometry)       
        #self.rb.setColor(QColor(60,151,255, 255)) # azul claro
        self.rb.setColor(QColor(100,100,255, 255))
        self.status = 0
        self.mover = 0
        self.point = None
        self.points = []

        #return None

        
    # Left click to place points. Right click to confirm.
     
    def keyPressEvent(self, e):
        if e.matches(QKeySequence.Undo):
            if self.rb.numberOfVertices() > 1:
                self.rb.removeLastPoint()


    def canvasPressEvent(self,e):
        if e.button() == Qt.LeftButton:
            if self.status == 0:
                #self.rb.reset( QGis.Polygon )                      # pyqgis2
                self.rb.reset(QgsWkbTypes.PolygonGeometry)
                self.status = 1

                self.mover = 0
                self.point = self.toMapCoordinates(e.pos())
                self.points.append(self.point)
                
            self.rb.addPoint(self.toMapCoordinates(e.pos()))
                       
            if self.mover == 2:
                self.point = self.toMapCoordinates(e.pos())
                self.points.append(self.point)                
                self.mover = 1
               
       
        else:
            if self.rb.numberOfVertices() > 2:
                self.status = 0 
                if self.mover != 1:
                    self.point = self.toMapCoordinates(e.pos())
                    self.points.append(self.point) 
                    
                if len(self.points) > 2:                              
                    self.points.append(self.points[0])                    
                    self.drawHole(self.points)
                   
                    self.points=[] 
                    self.reset()                
                    #return None
            else:                
                self.points=[] 
                self.reset()       
        #return None        
                  
    
    def canvasMoveEvent(self,e):
        if self.rb.numberOfVertices() > 0 and self.status == 1:
            self.rb.removeLastPoint(0)
            self.rb.addPoint(self.toMapCoordinates(e.pos())) 
            self.mover = 2     
         
        #return None

    def reset(self):
        self.status = 0
        self.rb.reset( True )
        

    def deactivate(self):
        self.rb.reset( True )
        QgsMapTool.deactivate(self)


    #*********************************************

    def crialista(self, lista, regex): 
        #regex = re.compile('[-+]?\d*\.\d+|\d+')

        xyzlist=[]  
        for i in range(len(lista)):
            coord = lista[i] 
            coord = re.findall(regex, coord)
            numcoord = len(coord)                        
            coordx = float(coord[0])
            coordy = float(coord[1])
            #if numcoord != 2:
                #coordz = float(coord[2])
            #else:
                #coordz = float(0)
            coordz = float(coord[2])

            #newcoord = QgsPointV2(QgsWKBTypes.PointZ,coordx, coordy, coordz)        # pyqgis2
            newcoord = QgsPoint(coordx, coordy, coordz, wkbType=QgsWkbTypes.PointZ)

            xyzlist.append(newcoord)   
       
        return xyzlist
    
    #*********************************************
    
    def retiracarater(self, geom): 
        ini = geom.find("(")            # position of the first "("    
        ini = ini + 1
        fim = geom.rfind(")")           # position of the last ")"
        listgeom = geom[ini:fim]        # considers the characters between the two positions
        return listgeom
        
    #*********************************************   

    def idw(self, x1 , y1, list, ncoord):
        
        sumValue = 0
        sumWeight = 0
        z1 = 0
        for k in range(ncoord):
            xe = list.xAt(k)
            ye = list.yAt(k)
            ze = list.zAt(k)
            
            distance = math.sqrt((x1-xe)**2 + (y1-ye)**2)
            if distance == 0:
                weight = 0
            else:
                weight = 1 / (distance**2)
            sumValue = sumValue + (ze*weight)
            sumWeight = sumWeight + weight

        if sumWeight == 0:
            z1 = 0
        else:    
            z1 = sumValue / sumWeight
        return z1
            

    #**********************************************************************
    #**********************************************************************    

    def drawHole(self, lista):
        regex = re.compile('[-+]?\d*\.\d+|\d+') 

        layer = self.iface.activeLayer()   
        """
        if not layer:
            QMessageBox.information(self.iface.mainWindow(), "Error", 'Layer failed to load!')
            return 

        if not layer.isEditable():
            QMessageBox.information(self.iface.mainWindow(), "Error", 'Layer is not editable.')
            return 
        """
        holelist=[]         
        for i in range(len(lista)):
            coordx = float(lista[i][0])
            coordy = float(lista[i][1])   
                     
            #newcoord=QgsPoint(coordx,coordy)             # pyqgis2   
            newcoord=QgsPointXY(coordx,coordy)    
            holelist.append(newcoord)  
            
        #hole = QgsGeometry.fromPolygon([holelist])       # pyqgis2
        hole = QgsGeometry.fromPolygonXY([holelist])  

        if not hole.isGeosValid():        
            QMessageBox.information(self.iface.mainWindow(), "Error", 'The drawn hole has an invalid geometry.')
            return               
        
        okHole = 0
        viewportPolygon = QgsGeometry().fromWkt(self.canvas.extent().asWktPolygon())
        #inViewFeats = layer.getFeatures(QgsFeatureRequest().setFilterRect(viewportPolygon.geometry().boundingBox()))       # pyqgis2
        inViewFeats = layer.getFeatures(QgsFeatureRequest().setFilterRect(viewportPolygon.boundingBox()))
        
        geom = None
        geom1 = None
        for feat in inViewFeats: 
            geom = feat.geometry()
            geom1 = geom.asPolygon()
            okHole = 0
            for point in lista:             # verifies that the vertices of the hole are contained in the feature
                if geom.contains(point) == False:  
                    okHole = 1  
                    geom = None
                    geom1 = None
                    break 
            if okHole == 0:
                break   
                   
        if okHole == 1 or geom == None:
            QMessageBox.information(self.iface.mainWindow(), "Error", "The feature does not contain the hole draw.")
            return 
        
        #geomV2 = geom.geometry()               # pyqgis2
        #geomV22 = geomV2.asWkt()               # pyqgis2
        geomV22 = geom.asWkt()        
       
        #**************************************************************************************
        #checks if hole intersects rings of selected polygon => "combine" hole + ring  and add ring to list delRings
        #**************************************************************************************  
        delRings = []                               
        
        cont = 0                                    # count rings
        for pol in geom1[1:]:                       # not consider exterior ring          
            cont += 1
            #ring = QgsGeometry.fromPolygon([pol])                      # pyqgis2
            ring = QgsGeometry.fromPolygonXY([pol])
            if hole.intersects(ring):               # QgsGeometry : intersects(QgsGeometry)
                delRings.append(cont)                
                hole = hole.combine(ring)           # QgsGeometry : combine(QgsGeometry)    

        if not hole.isGeosValid():        
            QMessageBox.information(self.iface.mainWindow(), "Error", 'The drawn hole has an invalid geometry.')
            return      
                               
        hole1=hole.asPolygon()             
		
        #holeV2 = hole.geometry()                    # pyqgis2
        #holeV22 = holeV2.asWkt()                    # pyqgis2
        holeV22 = hole.asWkt()
        
        #**********************************************************************
        # create polygon + hole
        #**********************************************************************  

        if (len(geom1)) == 0:    
            listgeomV22 = self.retiracarater(geomV22)
        else:               
            geomV22sub = self.retiracarater(geomV22)            
            geomV22sub1 = self.retiracarater(geomV22sub)   
            listgeomV22 = geomV22sub1.split("),(")           # remove the characters "),("             

        listvertex = []
        for i in range(len(listgeomV22)):
            vertex = listgeomV22[i]
            vertex = vertex.split(",")
            listvertex.append(vertex)
                
        geomlist=[] 
        for i in range(len(listvertex)):
            geomlist.append(self.crialista(listvertex[i], regex))       
            
        #chgfeat = QgsPolygonV2()               # pyqgis2
        chgfeat = QgsPolygon() 

        for i in range(len(geomlist)):
            #linestring_polygon = QgsLineStringV2()         # pyqgis2
            linestring_polygon = QgsLineString() 
            linestring_polygon.setPoints(geomlist[i])  
            
            if i == 0:
                ncoord = len(geomlist[0])
                ringext = linestring_polygon
                chgfeat.setExteriorRing(linestring_polygon) 
            else:
                flagx = 0
                for num in delRings:
                    if num == i:  
                        flagx = 1
                if flagx == 0:  
                    chgfeat.addInteriorRing(linestring_polygon)   
                    
        #**************************************************
        # transform hole 2D ===> hole 3D
        #**************************************************  
                     
        if (len(hole1)) == 0: 
            listgeomV22 = self.retiracarater(holeV22)
        else:               
            geomV22sub = self.retiracarater(holeV22)            
            geomV22sub1 = self.retiracarater(geomV22sub)   
            listgeomV22 = geomV22sub1.split("),(")                
        
        listvertex = []
        for i in range(len(listgeomV22)):
            vertex = listgeomV22[i]
            vertex = vertex.split(",")
            listvertex.append(vertex)           

        xylist=[]  
        lista = []
        regex = re.compile('[-+]?\d*\.\d+|\d+') 
        lista=listvertex[0]
        
        for i in range(len(lista)):             
            coord = lista[i] 
            coord = re.findall(regex, coord)
                                            
            xi = float(coord[0])                    
            yi = float(coord[1]) 
            newcoord =(xi,yi)
            xylist.append(newcoord) 

                
        holelist=[]     
        for j in range(len(xylist)):  
            x1 = xylist[j][0]
            y1 = xylist[j][1]

            zi = self.idw(x1, y1, ringext, ncoord)    # IDW - Inverse Distance Weighting Interpolation

            coordx = float(x1)                    
            coordy = float(y1)   
            coordz = float(zi)     

            #newcoord = QgsPointV2(QgsWKBTypes.PointZ,coordx, coordy, coordz)             # pyqgis2
            newcoord = QgsPoint(coordx, coordy, coordz, wkbType=QgsWkbTypes.PointZ)
            holelist.append(newcoord)          
       
	    
        #linestring_hole = QgsLineStringV2()            # pyqgis2
        #linestring_hole.setPoints(holelist)            # pyqgis2
        linestring_hole = QgsLineString()     
        linestring_hole = QgsLineString(holelist)  
    
        chgfeat.addInteriorRing(linestring_hole) 
        
        layer.startEditing()

        chggeom=QgsFeature()
        
        chggeom.setGeometry(QgsGeometry(chgfeat))                              
        
        chggeom.setFields(feat.fields())
       
        for field in feat.fields():
            chggeom.setAttribute(field.name(), feat[field.name()])
               
        #layer.addFeature(chggeom, True)            # pyqgis2
        layer.addFeature(chggeom)
        layer.deleteFeature(feat.id())       
                        
        layer.triggerRepaint()
       
        #self.iface.messageBar().pushMessage("CIGeoE Hole 3D", "Hole 3D done" , level=QgsMessageBar.INFO, duration=2)   # pyqgis2
        self.iface.messageBar().pushMessage("CIGeoE Hole 3D", "Hole in 3D done" , level=Qgis.Info, duration=2)

        
