Holes 3D is a plugin for QGis.

Plugin to create holes in polygons. If polygon has 3D coordinates, each hole vertex will also have 3D coordinates, with the Z being calculated using Inverse Distance Weighting (IDW) interpolation.

Notes:
- layer must contain only polygons (and not multipolygons, or other geometry types);
- all hole vertices must lie inside the same polygon;
- layer EPSG must be the same of QGIS EPSG project (if in the project there are multiple layers with different EPSG, QGIS will reproject them on the fly to a common EPSG that is the one visible in the tag on the lower right corner of QGIS - that EPSG must match with the EPSG of the layer you are wanting to create holes in).

Usage:
- activate the plugin and set the target layer as editable;
![ALT](/images/image1.jpg)
- cursor will now be a cross and you can press the mouse left button to start creating the hole inside a polygon;
![ALT](/images/image2.jpg)
- in the last vertex, press the mouse right button to close the polygon hole. All hole polygon vertices must lie inside the same polygon;
![ALT](/images/image3.jpg)
- if you create another hole inside that polygon, if it overlaps a previous hole, the result will be a larger single hole; 
![ALT](/images/image4.jpg)
![ALT](/images/image5.jpg)

Centro de Informação Geoespacial do Exército (www.igeoe.pt)

Portugal